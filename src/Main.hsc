module Main where

import Control.Exception (try, Exception, SomeException, bracket)
import Control.Monad (when, unless)
import Control.Monad.Fix (fix)
import Data.Bits
import Data.Foldable (traverse_, for_)
import Data.Functor (void)
import Foreign hiding (void)
import Foreign.C
import Foreign.Storable
import System.IO (hSetBuffering, stdout, BufferMode(..))
import System.Win32 hiding (try)
import System.Win32.Word
import Text.Read (readEither)
import Text.Printf (printf)
#include <windows.h>
#include "sysinfoapi.h"

bytesToKibytes :: DWORDLONG -> Double
bytesToKibytes = (/ 1024) . fromIntegral

bytesToMibytes :: DWORDLONG -> Double
bytesToMibytes =  (/ 1024) . (/ 1024) . fromIntegral

bytesToGigibytes :: DWORDLONG -> Double
bytesToGigibytes = (/ 1024) . (/ 1024) . (/ 1024) . fromIntegral

printMemory :: DWORDLONG -> String
printMemory v
  | v < 1024 = printf "%i bytes" v
  | v < 1024 * 1024 = printf "%.04f KiB" . bytesToKibytes $ v
  | v < 1024 * 1024 * 1024 = printf "%.04f MiB" . bytesToMibytes $ v
  | otherwise = printf "%.04f GiB" . bytesToGigibytes $ v

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering -- Allow to ask user for input on the same line
  i <- promptUntil (\a -> a > 0 && a < 2) "Choose either old lab (0) or new lab (1) tasks"
  if i > 0 then newLabPart else oldLabPart

printMemoryInfo :: IO MEMORYSTATUSEX
printMemoryInfo = do
  a@MEMORYSTATUSEX{..} <- globalMemoryStatusEx
  putStrLn $ unlines [
      "Percentage of used physical memory:          " <> show dwMemoryLoad          <> "%"
    , "Actual physical memory:                      " <> printMemory ullTotalPhys
    , "Physical memory currently available:         " <> printMemory ullAvailPhys
    , "Current committed memory limit:              " <> printMemory ullTotalPageFile
    , "Maximum amount of memory can commit:         " <> printMemory ullAvailPageFile
    , "Size of the user-mode virtual address space: " <> printMemory ullTotalVirtual
    , "Unreserved/uncommitted virtual memory:       " <> printMemory ullAvailVirtual
    ]
  pure a

newLabPart :: IO ()
newLabPart = do
  -- Perform 1-6 without reserved memory for part 6.
  labPart1to6 Nothing $ \partsPartial _ -> do
    pausePrompt "Part 7. Освободить оставшиеся выделенные впункте 2 блоки"
    traverse_ (\x -> virtualFree x 0 mEM_RELEASE) partsPartial

  putStrLn "Part 8. Повторить пункт 1, сравнив результаты сполученными внем ранее. "
  mem2 <- printMemoryInfo

  pausePrompt "Part 9. Зарезервировать область памяти того же размера, что и в пункте 6. "
  withPart9 mem2 $ \memQuarterRes -> do
    memQuarter <- maybe (fail "To conitnue need to reserve memory! Exiting") pure memQuarterRes
    pausePrompt "Part 10. Повторить пункты 1-6."
    -- Repeat with reserved memory for part 6
    labPart1to6 (Just memQuarter) $ \partsPartial _ -> do
      putStrLn "Part 11. Определить особенности наблюдавшихся состояний памяти процесс..."

      pausePrompt "Освободить оставшиеся выделенные впункте 2 блоки"
      traverse_ (\x -> virtualFree x 0 mEM_RELEASE) partsPartial

      pausePrompt "Press any key to release 1/4 memory block"

  pausePrompt "Press any key to exit..."

-- | Perform lab parts and use provided reserved memory for part 6. Pass to lambda
-- partial deallocated memory parts from part 2 and allocated memory from part 6 ('Nothing' means we failed to allocate).
labPart1to6 :: Maybe Addr -> ([Addr] -> Maybe Addr -> IO a) -> IO a
labPart1to6 mreserved f = do
  putStrLn "Part 1. Получить инапечатать общий идоступный объем виртуальной памяти"
  _ <- printMemoryInfo

  pausePrompt "Part 2. Использовать всю  доступную  ВП. Print any key..."
  parts <- allocateThemAll

  putStrLn "Part 3. Повторить пункт 1, сравнить доступный объем памяти со 100МБ"
  _ <- printMemoryInfo

  pausePrompt "Part 4. Освободить каждый второй из выделенных впункте 2 блоков. Print any key..."
  partsPartial <- freeEvery 2 parts

  putStrLn "Part 5. Повторить пункт 1, обратив внимание наобъем доступной ВП. "
  mem <- printMemoryInfo

  pausePrompt "Part 6. Попытаться выделить блок размером 1/4 ВП..."
  labPart6 mem mreserved (f partsPartial)

allocateThemAll :: IO [Addr]
allocateThemAll = go []
  where
    size = 100 * 1024 * 1024
    go !ms = do
      mc <- try $ virtualAlloc nullPtr size mEM_COMMIT pAGE_READWRITE
      case mc of
        Left (e :: SomeException) -> do
          putStrLn $ "Allocation stop with: " <> show e
          pure ms
        Right m -> go (m : ms)

freeEvery :: Int -> [Addr] -> IO [Addr]
freeEvery n = go 0 []
  where
    go _ acc [] = pure acc
    go i !acc (x:xs)
      | i `mod` n == 0 = virtualFree x 0 mEM_RELEASE >> go (i+1) acc xs
      | otherwise = go (i+1) (x : acc) xs

labPart6 :: MEMORYSTATUSEX -> Maybe Addr -> (Maybe Addr -> IO a) -> IO a
labPart6 mem resMemory f = do
  let quarterSize = fromIntegral $ ullAvailVirtual mem `div` 4
      quarterSizeT = printMemory $ fromIntegral quarterSize
  tryWithVirtualReserved quarterSize resMemory $ \memQuarterRes -> do
    case memQuarterRes of
      Left (e :: SomeException) -> do
        putStrLn $ "Cannot allocated " <> quarterSizeT
        f Nothing
      Right memQuarter -> do
        putStrLn $ "Successfully allocated " <> quarterSizeT
        f $ Just memQuarter


withPart9 :: MEMORYSTATUSEX -> (Maybe Addr -> IO a) -> IO a
withPart9 mem f = do
  let quarterSize = fromIntegral $ ullAvailVirtual mem `div` 4
      quarterSizeT = printMemory $ fromIntegral quarterSize
  tryWithVirtual quarterSize mEM_RESERVE $ \memQuarterResE -> do
    case memQuarterResE of
      Left (e :: SomeException) -> do
        putStrLn $ "Cannot allocated " <> quarterSizeT
        f Nothing
      Right memQuarterRes -> do
        putStrLn $ "Successfully reserved " <> quarterSizeT
        f $ Just memQuarterRes

oldLabPart :: IO ()
oldLabPart = do
  n <- promptUntil (> 0) "Input natural number"
  calcInMemory n (*) -- Multiplication will calculate factorial as product [1 .. n]
  p1 <- promptUntil (> 0) "Input low bound of address range"
  p2 <- promptUntil (> p1) "Input high bound of address range"
  inspectMemory (intPtrToPtr  p1) (intPtrToPtr  p2) printInfo

-- | Calculate function in the memory. Lambda takes accumulator  current index
-- and calculated new accumulator.
calcInMemory :: Int -> (Int -> Int -> Int) -> IO ()
calcInMemory n f = do
  let size = fromIntegral $ n * offset
  withVirtual size mEM_COMMIT $ \p -> do
    let p' = castPtr p :: Ptr Int
    nf <- writeMem p' 1 1
    putStrLn $ "Factorial: " <> show nf
    readMem p' 1
  where
    offset = sizeOf (undefined :: Int)

    writeMem :: Ptr Int -> Int -> Int -> IO Int
    writeMem p acc i
      | i > n     = pure acc
      | otherwise = do
        let acc' = f acc i
        poke p acc'
        writeMem (plusPtr p offset) acc' (i+1)

    readMem :: Ptr Int -> Int -> IO ()
    readMem p i
      | i > n = pure ()
      | otherwise = do
        v <- peek p
        putStrLn $ "Memory addr " <> show p <> " value: " <> show v
        readMem (plusPtr p offset) (i+1)

-- | Inspect metainfo about memory from first to second address. First address
-- must be smaller than second one!
inspectMemory :: Addr -> Addr -> (MEMORY_BASIC_INFORMATION -> IO ()) -> IO ()
inspectMemory a b f = go a b
  where
  go p1 p2
    | p1 >= p2 = pure ()
    | otherwise = do
      info <- virtualQuery p1
      f info
      go (p1 `plusPtr` fromIntegral (mbiRegionSize info)) p2

printInfo :: MEMORY_BASIC_INFORMATION -> IO ()
printInfo MEMORY_BASIC_INFORMATION{..} = putStrLn $ unlines [
    "============================================================"
  , "Base address:           " <> show mbiBaseAddress
  , "Allocation base:        " <> show mbiAllocationBase
  , "Allocation protection:  " <> showProtect mbiAllocationProtect
  , "Region size:            " <> show mbiRegionSize
  , "State:                  " <> showState mbiState
  , "Protection:             " <> showProtect mbiAllocationProtect
  , "Type:                   " <> showType mbiAllocationProtect
  ]
  where
    showProtect i
      | i == pAGE_EXECUTE = "PAGE_EXECUTE"
      | i == pAGE_EXECUTE_READ = "PAGE_EXECUTE_READ"
      | i == pAGE_EXECUTE_READWRITE = "PAGE_EXECUTE_READWRITE"
      | i == pAGE_EXECUTE_WRITECOPY = "PAGE_EXECUTE_WRITECOPY"
      | i == pAGE_NOACCESS = "PAGE_NOACCESS"
      | i == pAGE_READONLY = "PAGE_READONLY"
      | i == pAGE_READWRITE = "PAGE_READWRITE"
      | i == pAGE_WRITECOPY = "PAGE_WRITECOPY"
      | i == pAGE_TARGETS_INVALID = "PAGE_TARGETS_INVALID"
      | i == pAGE_TARGETS_NO_UPDATE = "PAGE_TARGETS_NO_UPDATE"
      | otherwise = show i
    showState i
      | i == mEM_COMMIT = "MEM_COMMIT"
      | i == mEM_FREE = "MEM_FREE"
      | i == mEM_RESERVE = "MEM_RESERVE"
      | otherwise = show i
    showType i
      | i == mEM_IMAGE = "MEM_IMAGE"
      | i == mEM_MAPPED = "MEM_MAPPED"
      | i == mEM_PRIVATE = "MEM_PRIVATE"
      | otherwise = show i

pAGE_EXECUTE_WRITECOPY :: ProtectFlags
pAGE_EXECUTE_WRITECOPY = 0x80

pAGE_WRITECOPY :: ProtectFlags
pAGE_WRITECOPY = 0x08

pAGE_TARGETS_INVALID :: ProtectFlags
pAGE_TARGETS_INVALID = 0x40000000

pAGE_TARGETS_NO_UPDATE :: ProtectFlags
pAGE_TARGETS_NO_UPDATE = 0x40000000

mEM_FREE :: VirtualAllocFlags
mEM_FREE = 0x10000

mEM_IMAGE :: VirtualAllocFlags
mEM_IMAGE = 0x1000000

mEM_MAPPED :: VirtualAllocFlags
mEM_MAPPED = 0x40000

mEM_PRIVATE :: VirtualAllocFlags
mEM_PRIVATE = 0x20000


-- | Allocate virtual memory of given size and pass it to lambda. Do not return
-- pointer to the memory from the closure as it will be destroyed as soon as
-- the lamda ends.
withVirtual :: DWORD -> VirtualAllocFlags -> (Addr -> IO a) -> IO a
withVirtual size allocFlag = bracket
  (virtualAlloc nullPtr size allocFlag pAGE_READWRITE)
  (\p -> virtualFree p 0 mEM_RELEASE)

-- | Same as 'tryWithVirtual' but use reserved memory preallocated if you pass second
-- argument with pointer to the reserved memory.
tryWithVirtualReserved :: Exception e => DWORD -> Maybe Addr -> (Either e Addr -> IO a) -> IO a
tryWithVirtualReserved size Nothing = tryWithVirtual size mEM_COMMIT
tryWithVirtualReserved size (Just raddr) = bracket
  (try $ virtualAlloc raddr size mEM_COMMIT pAGE_READWRITE)
  (traverse_ (\p -> virtualFree p size mEM_DECOMMIT))

-- | Same as 'withVirtual' but return 'Left' if allocation failed with exception.
tryWithVirtual :: Exception e => DWORD -> VirtualAllocFlags -> (Either e Addr -> IO a) -> IO a
tryWithVirtual size allocFlag = bracket
  (try $ virtualAlloc nullPtr size allocFlag pAGE_READWRITE)
  (traverse_ (\p -> virtualFree p 0 mEM_RELEASE))

-- | Ask user to input string
prompt :: String -> IO String
prompt title = do
  putStr $ title ++ ": "
  getLine

-- | Prompt user any value that has 'Read' instance. Repeat if cannot parse user
-- input and validate it via predicate.
promptUntil :: Read a => (a -> Bool) -> String -> IO a
promptUntil f title = fix $ \next -> do
  s <- prompt title
  case readEither s of
    Left er -> do
      putStrLn $ "Failed to parse answer: " ++ er
      next
    Right a -> if f a then pure a else do
      putStrLn $ "Invalid value! Try again..."
      next

-- | Prompt that can be passed with any key
pausePrompt :: String -> IO ()
pausePrompt title = do
  putStr title
  void $ getChar

-- | Get last error string
getError :: IO String
getError = do
  i <- getLastError
  bracket (getErrorMessage i) localFree peekTString

virtualQuery :: Addr -> IO MEMORY_BASIC_INFORMATION
virtualQuery lpAddress = alloca $ \lpBuffer -> do
  let dwLength = fromIntegral $ sizeOf (undefined :: MEMORY_BASIC_INFORMATION)
  s <- c_VirtualQuery lpAddress lpBuffer dwLength
  when (s == 0) $ do
    msg <- getError
    putStrLn msg
    fail msg
  peek lpBuffer

foreign import stdcall unsafe "windows.h VirtualQuery"
  c_VirtualQuery :: LPVOID -> Ptr MEMORY_BASIC_INFORMATION -> SIZE_T -> IO DWORD

globalMemoryStatusEx :: IO MEMORYSTATUSEX
globalMemoryStatusEx = alloca $ \statusPtr -> do
  poke (castPtr statusPtr) $ sizeOf (undefined :: MEMORYSTATUSEX) -- Fill dwLength
  res <- c_GlobalMemoryStatusEx statusPtr
  unless res $ do
    msg <- getError
    putStrLn msg
    fail msg
  peek statusPtr

data MEMORYSTATUSEX = MEMORYSTATUSEX {
    dwLength                :: !DWORD
  , dwMemoryLoad            :: !DWORD
  , ullTotalPhys            :: !DWORDLONG
  , ullAvailPhys            :: !DWORDLONG
  , ullTotalPageFile        :: !DWORDLONG
  , ullAvailPageFile        :: !DWORDLONG
  , ullTotalVirtual         :: !DWORDLONG
  , ullAvailVirtual         :: !DWORDLONG
  , ullAvailExtendedVirtual :: !DWORDLONG
  }

instance Storable MEMORYSTATUSEX where
    sizeOf _ = #size MEMORYSTATUSEX
    alignment _ = #alignment MEMORYSTATUSEX
    poke buf mbi = do
        (#poke MEMORYSTATUSEX, dwLength)                buf (dwLength mbi)
        (#poke MEMORYSTATUSEX, dwMemoryLoad)            buf (dwMemoryLoad mbi)
        (#poke MEMORYSTATUSEX, ullTotalPhys)            buf (ullTotalPhys mbi)
        (#poke MEMORYSTATUSEX, ullAvailPhys)            buf (ullAvailPhys mbi)
        (#poke MEMORYSTATUSEX, ullTotalPageFile)        buf (ullTotalPageFile mbi)
        (#poke MEMORYSTATUSEX, ullAvailPageFile)        buf (ullAvailPageFile mbi)
        (#poke MEMORYSTATUSEX, ullTotalVirtual)         buf (ullTotalVirtual mbi)
        (#poke MEMORYSTATUSEX, ullAvailVirtual)         buf (ullAvailVirtual mbi)
        (#poke MEMORYSTATUSEX, ullAvailExtendedVirtual) buf (ullAvailExtendedVirtual mbi)
    peek buf = do
        dwLength                <- (#peek MEMORYSTATUSEX, dwLength)                buf
        dwMemoryLoad            <- (#peek MEMORYSTATUSEX, dwMemoryLoad)            buf
        ullTotalPhys            <- (#peek MEMORYSTATUSEX, ullTotalPhys)            buf
        ullAvailPhys            <- (#peek MEMORYSTATUSEX, ullAvailPhys)            buf
        ullTotalPageFile        <- (#peek MEMORYSTATUSEX, ullTotalPageFile)        buf
        ullAvailPageFile        <- (#peek MEMORYSTATUSEX, ullAvailPageFile)        buf
        ullTotalVirtual         <- (#peek MEMORYSTATUSEX, ullTotalVirtual)         buf
        ullAvailVirtual         <- (#peek MEMORYSTATUSEX, ullAvailVirtual)         buf
        ullAvailExtendedVirtual <- (#peek MEMORYSTATUSEX, ullAvailExtendedVirtual) buf
        pure MEMORYSTATUSEX{..}

foreign import stdcall unsafe "sysinfoapi.h GlobalMemoryStatusEx"
  c_GlobalMemoryStatusEx :: Ptr MEMORYSTATUSEX -> IO BOOL
